#ifndef PILE_H
#define PILE_H

/* ------------------------------------ */
/* 		Type d'élément de la pile   	*/
/* ------------------------------------ */

#ifndef DONNEE_T
#define DONNEE_T
    typedef int donnee_t;
#endif

/* ------------------------------------ */
/* 			Structure de la pile   		*/
/* ------------------------------------ */


typedef struct pile {
    int         taille;
    int         nb_elements;
    donnee_t    *donnees;
} pile_t;


/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */

pile_t      *initPile(int);
donnee_t    tetePile(pile_t *);
int         estPileVide(pile_t *);
int         estPilePleine(pile_t *);
void        libererPile(pile_t *);
donnee_t 	depiler(pile_t *, int *);
int         empiler(pile_t *, donnee_t);
void        afficherPile(pile_t *);

#endif
