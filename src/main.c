#include <stdio.h>
#include <stdlib.h>
#include "pile.h"
#include "file.h"

/* -------------------------------------------------------------------- */
/* Echanger             Echange les valeurs de deux variables           */
/*                                                                      */
/* En entrée: val1 : pointeur sur la première variable                  */
/*            val2 : pointeur sur la seconde variable                   */
/*                                                                      */
/* En sortie: les pointeurs en entrée                                   */
/* -------------------------------------------------------------------- */
void Echanger(donnee_t * val1, donnee_t * val2)
{
    donnee_t tmp;

    tmp = *val1;
    *val1 = *val2;
    *val2 = tmp;
}

/* -------------------------------------------------------------------- */
/* TRUC  Affiche les anagrammes d'un nombre de façon récursive          */
/*                                                                      */
/* En entrée: i : indice actuel dans le tableau                         */
/*            n : taille du tableau                                     */
/*            tab : tableau contenant les chiffres composant le nombre  */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void TRUC(int i, int n, int* tab){
    int j;

    if(i == n)
    {
        for(j = 0; j <= n; j++)
            printf("%d ", tab[j]);
        printf("\n");
    }
    else
        for(j = i; j <= n; j++){
            Echanger(&tab[i],&tab[j]);
            TRUC(i+1, n, tab);
            Echanger(&tab[i],&tab[j]);
        }
}

/* -------------------------------------------------------------------- */
/* TRUC_ITERATIF  Affiche les anagrammes d'un nombre de façon iterative */
/*                                                                      */
/* En entrée: i : indice actuel dans le tableau                         */
/*            n : taille du tableau                                     */
/*            tab : tableau contenant les chiffres composant le nombre  */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void TRUC_ITERATIF(int i, int n, int* tab){
    int     j, stop=0, code;
    pile_t  *pile = initPile(2*n);

    i--;
    n--;
    j=i;
    while(!stop)
    {
        /*printf("Je fais l'appel de TRUC_ITERATIF(%d,%d,tab)\n", i+1, n+1);*/
        if(i == n)
        {
            for(j = 0; j <= n; j++)
                printf("%d ", tab[j]);
            printf("\n");
            if(!estPileVide(pile))
            {
                j = depiler(pile, &code);
                i = depiler(pile, &code);
                Echanger(&tab[i],&tab[j]);
                j++;
            }
            else
                stop = 1;
        }
        else
        {
            if(j <= n)
            {
                Echanger(&tab[i],&tab[j]); 
                empiler(pile, i);
                empiler(pile, j);
                i++;
                j=i;
            }
            else
            {
                while(j > n && !stop)
                {
                    if(!estPileVide(pile))
                    {
                        j = depiler(pile, &code);
                        i = depiler(pile, &code);
                        Echanger(&tab[i],&tab[j]);
                        j++;
                    }
                    else
                        stop = 1;
                }
            }
        }
    }
    libererPile(pile);
}



/* -------------------------------------------------------------------- */
/* main        Programme principal                                      */
/*                                                                      */
/* En entrée: argc : taille du tableau argv                             */
/*            argv : tableau des arguments du programme                 */
/*                                                                      */
/* En sortie: code de retour de la fonction pour le processus entier    */
/* -------------------------------------------------------------------- */
int main()
{   
    pile_t  *pile = initPile(2); 
    file_t  *file = initFile(2);
    int     tab[3];
    int code;

    /*TESTS SUR LES PILES*/
    if(pile != NULL){

        /*TESTS SUR LES PILES VIDES*/
        puts("TEST afficherPile (quand pile vide)");
        afficherPile(pile);

        printf("\nTEST estPileVide (quand pile vide) [vide = 1, non-vide = 0] => %d\n", estPileVide(pile));
        printf("\nTEST estPilePleine (quand pile vide) [pleine = 1, non-pleine = 0] => %d\n", estPilePleine(pile));
        depiler(pile, &code);
        printf("\nTEST depiler (quand pile vide) [échec = 1, succès = 0] => %d\n", code);

        libererPile(pile);
        pile = initPile(2); 

        printf("\nTEST empiler (quand pile vide) [échec = 1, succès = 0] => %d\n", empiler(pile, 3));
        empiler(pile, 4);
        afficherPile(pile);

        /*TESTS SUR LES PILES PLEINES*/
        puts("\nTEST afficherPile (quand pile pleine)");
        afficherPile(pile);

        printf("\nTEST estPileVide (quand pile pleine) [vide = 1, non-vide = 0] => %d\n", estPileVide(pile));
        printf("\nTEST estPilePleine (quand pile pleine) [pleine = 1, non-pleine = 0] => %d\n", estPilePleine(pile));
        printf("\nTEST tetePile (quand pile pleine) : %d\n", tetePile(pile));
        printf("\nTEST empiler (quand pile pleine) [échec = 1, succès = 0] => %d\n", empiler(pile, 9));
        printf("\nTEST depiler (quand pile pleine) => %d\n", depiler(pile, &code));
        printf("[échec = 1, succès = 0] => %d\n", code);
        
        libererPile(pile);
    }

    /*TESTS SUR LES FILES*/
    if(file != NULL)
    {
    	/*TESTS SUR LES FILES VIDES*/
        puts("TEST afficherFile (quand file vide)");
        afficherFile(file);

        printf("\nTEST estFileVide (quand file vide) [vide = 1, non-vide = 0] => %d\n", estFileVide(file));
        printf("\nTEST estFilePleine (quand file vide) [pleine = 1, non-pleine = 0] => %d\n", estFilePleine(file));
        defiler(file, &code);
        printf("\nTEST defiler (quand file vide) [échec = 1, succès = 0] => %d\n", code);

        libererFile(file);
        file = initFile(2); 

        printf("\nTEST enfiler (quand file vide) [échec = 1, succès = 0] => %d\n", enfiler(file, 3));
        enfiler(file, 4);
        afficherFile(file);

        /*TESTS SUR LES FILES PLEINES*/
        puts("\nTEST afficherFile (quand file pleine)");
        afficherFile(file);

        printf("\nTEST estFileVide (quand file pleine) [vide = 1, non-vide = 0] => %d\n", estFileVide(file));
        printf("\nTEST estFilePleine (quand file pleine) [pleine = 1, non-pleine = 0] => %d\n", estFilePleine(file));
        printf("\nTEST teteFile (quand file pleine) : %d\n", teteFile(file));
        printf("\nTEST enfiler (quand file pleine) [échec = 1, succès = 0] => %d\n", enfiler(file, 9));
        printf("\nTEST defiler (quand file pleine) => %d\n", defiler(file, &code));
        printf("[échec = 1, succès = 0] => %d\n", code);
        
        libererFile(file);
    }

    /*TESTS DE TRUC*/
    tab[0] = 2;
    tab[1] = 5;
    tab[2] = 9;
    puts("\nAppel de TRUC");
    TRUC(0, 2, tab);
    puts("Appel de TRUC_ITERATIF");
    TRUC_ITERATIF(1, 3, tab);

    return 0;
}
