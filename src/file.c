#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include "file.h"

/* -------------------------------------------------------------------- */
/* initFile        Initialise la structure de la file                   */
/*                                                                      */
/* En entrée: rien                                                      */
/*                                                                      */
/* En sortie: un pointeur vers la structure de la file                  */
/* -------------------------------------------------------------------- */
file_t *initFile(int taille){
    file_t *file = malloc(sizeof(file_t));
    if(file != NULL)
    {
        file->taille = taille;
        file->nb_elements = 0;
        file->donnees = malloc(taille*sizeof(donnee_t));
        if(file->donnees != NULL)
        {
            file->deb = file->donnees;
            file->fin = file->donnees + taille - 1;
        }
       else 
        {
	       free(file);
	       file = NULL;	
	   }           
    }
    return file;
}

/* -------------------------------------------------------------------- */
/* teteFile       Retourne l'élément de tête de la file                 */
/*                                                                      */
/* En entrée: file : la file dont on veut retourner l'élément de tête   */
/*                                                                      */
/* En sortie: l'élément de tête de la file                              */
/* -------------------------------------------------------------------- */
donnee_t teteFile(file_t *file){
	return *(file->deb);
}

/* -------------------------------------------------------------------- */
/* estFileVide       Vérifie si la file est vide                        */
/*                                                                      */
/* En entrée: file : la file à tester                                   */
/*                                                                      */
/* En sortie: 1 si vide, 0 sinon                                        */
/* -------------------------------------------------------------------- */
int estFileVide(file_t *file) {
    return file->nb_elements <= 0;
}

/* -------------------------------------------------------------------- */
/* estFilePleine       Vérifie si la file est pleine                    */
/*                                                                      */
/* En entrée: file : la file à tester                                   */
/*                                                                      */
/* En sortie: 1 si pleine, 0 sinon                                      */
/* -------------------------------------------------------------------- */
int estFilePleine(file_t *file) {
    return file->nb_elements >= file->taille;
}

/* -------------------------------------------------------------------- */
/* libererFile       Rend la mémoire occupée par la file                */
/*                                                                      */
/* En entrée: file : la file à vider                                    */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void libererFile(file_t *file){
    free(file->donnees);
    free(file);
}

/* -------------------------------------------------------------------- */
/* defiler       Défile un élément de la file                           */
/*                                                                      */
/* En entrée: file : la file dont on veut enlever un élément            */
/*            code : pointeur du code d'erreur à retourner              */
/*                                                                      */
/* En sortie: l'élément défilé                                          */
/* -------------------------------------------------------------------- */
donnee_t defiler(file_t *file, int *code){
    donnee_t res;

    if(!estFileVide(file)){
        res = *(file->deb);
        file->deb = (file->deb - file->donnees + 1)%file->taille + file->donnees;         
        --(file->nb_elements);  
        *code = EXIT_SUCCESS;
    }
    else
        *code = EXIT_FAILURE;

    return res;
}

/* -------------------------------------------------------------------- */
/* enfiler       Enfile un élément dans la file                         */
/*                                                                      */
/* En entrée: file : la file dans laquelle on veut enfiler un élément   */
/*            elmt : l'élément à enfiler                                */
/*                                                                      */
/* En sortie: un code de retour                                         */
/* -------------------------------------------------------------------- */
int enfiler(file_t *file, donnee_t elmt){
    int code = EXIT_FAILURE;

    if(!estFilePleine(file)){
        file->fin = (file->fin - file->donnees + 1)%file->taille + file->donnees;   
        *(file->fin) = elmt;
        ++(file->nb_elements);
        code = EXIT_SUCCESS;
    }

    return code;
}

/* -------------------------------------------------------------------- */
/* afficherFile       Affiche la file                                   */
/*                                                                      */
/* En entrée: file : la file à afficher                                 */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void afficherFile(file_t *file){
    int i;
    puts("Affichage de la file");
    for(i = 0; i < file->nb_elements; ++i){
        printf("%d ", *((file->deb - file->donnees + i)%file->taille + file->donnees));
    }
    printf("\n");
    puts("Fin d'affichage de la file");
}
