#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include "pile.h"
#include "utils.h"

/* -------------------------------------------------------------------- */
/* initPile        Initialise la structure de la pile                   */
/*                                                                      */
/* En entrée: rien                                                      */
/*                                                                      */
/* En sortie: un pointeur vers la structure de la pile                  */
/* -------------------------------------------------------------------- */
pile_t* initPile(int taille){
    pile_t* pile = sddalloc(sizeof(pile_t));
    pile->taille = taille;
    pile->nb_elements = 0;
    pile->donnees = sddalloc(taille * sizeof(donnee_t));
    return pile;
}

/* -------------------------------------------------------------------- */
/* tetePile       Retourne l'élément de tête de la pile                 */
/*                                                                      */
/* En entrée: pile : la pile dont on veut retourner l'élément de tête   */
/*                                                                      */
/* En sortie: l'élément de tête de la pile                              */
/* -------------------------------------------------------------------- */
donnee_t tetePile(pile_t *pile){
	return pile->donnees[pile->taille - pile->nb_elements];
}

/* -------------------------------------------------------------------- */
/* estPileVide       Vérifie si la pile est vide                        */
/*                                                                      */
/* En entrée: pile : la pile à tester                                   */
/*                                                                      */
/* En sortie: 1 si vide, 0 sinon                                        */
/* -------------------------------------------------------------------- */
int estPileVide(pile_t* pile) {
    return pile->nb_elements <= 0;
}

/* -------------------------------------------------------------------- */
/* estPilePleine       Vérifie si la pile est pleine                    */
/*                                                                      */
/* En entrée: pile : la pile à tester                                   */
/*                                                                      */
/* En sortie: 1 si pleine, 0 sinon                                      */
/* -------------------------------------------------------------------- */
int estPilePleine(pile_t* pile) {
    return pile->nb_elements >= pile->taille;
}

/* -------------------------------------------------------------------- */
/* libererPile       Rend la mémoire occupée par la pile                */
/*                                                                      */
/* En entrée: pile : la pile à vider                                    */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void libererPile(pile_t* pile){
    free(pile->donnees);
    free(pile);
}

/* -------------------------------------------------------------------- */
/* depiler       Dépile un élément de la pile                           */
/*                                                                      */
/* En entrée: pile : la pile dont on veut enlever un élément            */
/*            code : pointeur sur le code d'erreur à retourner          */
/*                                                                      */
/* En sortie: l'élément dépilé                                          */
/* -------------------------------------------------------------------- */
donnee_t depiler(pile_t* pile, int *code){
    donnee_t res;

    if(!estPileVide(pile)){
        res = pile->donnees[pile->taille - pile->nb_elements];
        --(pile->nb_elements);
        *code = EXIT_SUCCESS;
    }
    else
        *code = EXIT_FAILURE;

    return res;
}

/* -------------------------------------------------------------------- */
/* empiler       Empile un élément dans la pile                         */
/*                                                                      */
/* En entrée: pile : la pile dans laquelle on veut empiler un élément   */
/*            elmt : l'élément à empiler                                */
/*                                                                      */
/* En sortie: un code de retour                                         */
/* -------------------------------------------------------------------- */
int empiler(pile_t* pile, donnee_t elmt){
    int code = EXIT_FAILURE;
    if(!estPilePleine(pile)){
        pile->donnees[pile->taille - pile->nb_elements - 1] = elmt;
        ++(pile->nb_elements);
        code = EXIT_SUCCESS;
    }
    return code;
}

/* -------------------------------------------------------------------- */
/* afficherPile       Affiche la pile                                   */
/*                                                                      */
/* En entrée: pile : la pile à afficher                                 */
/*                                                                      */
/* En sortie: rien                                                      */
/* -------------------------------------------------------------------- */
void afficherPile(pile_t* pile){
    int i;
    puts("Affichage de la pile");
    for(i = pile->nb_elements-1; i >= 0; --i)
        printf("%d\n", pile->donnees[pile->taille - i - 1]);
    puts("Fin d'affichage de la pile");
}
