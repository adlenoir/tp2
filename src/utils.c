#include <stdio.h>
#include <stdlib.h>

/* -------------------------------------------------------------------- */
/* sddalloc          		wrapper de gestion d'erreurs pour malloc    */
/*                                                                      */
/* En entrée: size: taille à allouer								    */
/*                                                                      */
/* En sortie: Un pointeur pointant vers la cellule allouée			    */
/* -------------------------------------------------------------------- */
void *sddalloc(size_t size)
{
    void *alloc = malloc(size);
    if (!alloc)
    {
        fprintf(stderr, "Error while trying malloc !");
        exit(1);
    }
    return alloc;
}