#ifndef FILE_H
#define FILE_H


/* ------------------------------------ */
/* 		Type d'élément de la file   	*/
/* ------------------------------------ */

#ifndef DONNEE_T
#define DONNEE_T
    typedef int donnee_t;
#endif

/* ------------------------------------ */
/* 			Structure de la file   		*/
/* ------------------------------------ */

typedef struct file {
    int         taille;
    int         nb_elements;
    donnee_t    *donnees;
    donnee_t    *deb;
    donnee_t    *fin;
} file_t;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */

file_t      *initFile(int);
donnee_t    teteFile(file_t *);
int         estFileVide(file_t *);
int         estFilePleine(file_t *);
void        libererFile(file_t *);
donnee_t 	defiler(file_t *, int *);
int         enfiler(file_t *, donnee_t);
void        afficherFile(file_t *);

#endif
